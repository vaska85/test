$('.switcher').on('change', function (){
    let producType = $(this).val();
    $('.dvd, .book, .furniture').css('display', 'none');
    $('.dvd, .book, .furniture').prop('required', false);
    if(producType !== '') {
        $('.'+ producType).css('display', 'inline-block');
        $('.'+ producType).prop('required', true);
    }
});