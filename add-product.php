<?php
    require('db_class.php');
    $obj = new Add();
    $con = new Connection();
        if (isset($_POST['submit'])) {
            $obj->getdata();
            if ($obj->connection) {
                echo '<meta http-equiv="refresh" content="0; url=index.php">';
            }
        }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="style_add_item.css">
        <title>Add a Product</title>
    </head>
    <body>
        <header>
            <div class="header">
                <h1>Product Add</h1>
                <hr>
            </div>
        </header>
        <div class="form-container form">
            <form id="product_form" method="post">
                <div class="base-input">
                    <label for="sku">SKU#</label>
                    <input type="text" name="sku" id="sku" required>
                    <br>
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" required>
                    <br>
                    <label for="price">Price ($)</label>
                    <input type="number" name="price" id="price" required>
                    <br>
                </div>
                <div class="typeswitcher">
                    <label for="typeswitcher">Typeswitcher</label>
                    <select name="typeswitcher" id="productType" class="switcher" required>
                        <option value="">Choose Type</option>
                        <option value="dvd" id="">DVD</option>
                        <option value="book" id="">Book</option>
                        <option value="furniture" id="">Furniture</option>
                    </select>
                </div>
                <div class="optional-input">
                    <div class="dvd">
                        <label for="size">Size (MB)</label>
                        <input type="number" name="size" id="size" class="dvd">
                        <p><span>Please, provide size in Megabytes</span></p>
                    </div>
                    <div class="book">
                        <label for="weight">Weight</label>
                        <input type="number" name="weight" id="weight" class="book">
                        <br>
                        <p><span>Please, provide weight</span></p>
                    </div>
                    <div class="furniture">
                        <label for="height">Height</label>
                        <input type="number" name="height" id="height" class="furniture">
                        <br>
                        <label for="width">Width</label>
                        <input type="number" name="width" id="width" class="furniture">
                        <br>
                        <label for="length">Length</label>
                        <input type="number" name="length" id="length" class="furniture">
                        <br>
                        <p><span>Please, provide dimensions</span></p>
                    </div>
                </div>
                <div class="buttons">
                    <button type="submit" class="btn btn-primary" name="submit">Save</button>
                    <a class="btn btn-danger" href="index.php"> Cancel </a>
                </div>
            </form>
        </div>
        <footer>
            <hr>
            <small>Scandiweb Test Assignment</small>
        </footer>
    </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript" src="script.js"></script>
</html>