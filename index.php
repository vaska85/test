<?php 
    require('db_class.php');
    if (isset($_POST['submit'])) {
        $del = new Delete();
        $del->delete();
    }
?>

<html>
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="style.css">
    </head>
    <header>
        <h1>Product List</h1>
        <hr>
    </header>
    <body>
        <div class="form-container">
            <form action="" method="post">
                <div class="product-list">
                    <?php
                        $allitems = new Display();
                    ?>
                </div>
                <div class="buttons">
                    <a class="btn btn-primary" href="add-product.php">ADD</a>
                    <button type="submit" name="submit" class="btn btn-danger">MASS DELETE</button>
                </div>
            </form>
        </div>
        <footer>
            <hr>
            <p>Scandiweb Test Assignment</p>
        </footer>
    </body>
    <script src="script.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</html>